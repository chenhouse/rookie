# rookie

新人所需學習的知識之處！

> rookie : A person with little experience who is just starting to work in a job or to play on a sports team

## Essential ##

- Sportsbook Domain
    - [Bet365](https://www.bet365.com)
    - [奧創娛樂 - 博弈百科](https://www.ultraegaming.com/tw/blog/category/博弈百科/體育盤口規則)
    - [PaddyPower - Bet Calculator](http://bet-calculator.paddypower.com/)
- Markdown
    - [Markdown文件](https://markdown.tw/)
    - [GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)
    - [Markdown Editor - 熊掌記](https://itunes.apple.com/tw/app/%E7%86%8A%E6%8E%8C%E8%A8%98/id1016366447?mt=8)
- Version Control
    - [Git](https://git-scm.com/)
    - [Gitlab](https://gitlab.com/)
    - [連猴子都能懂的Git入門指南| 貝格樂（Backlog）](https://backlog.com/git-tutorial/tw/)
    - [為你自己學 Git](https://gitbook.tw/)
    - [SourceTree](https://www.sourcetreeapp.com/)
- Agile
    - [Agile Manifesto](https://agilemanifesto.org/)
        - [Lean Software Development 準則](http://kojenchieh.pixnet.net/blog/post/335437076-%E7%B2%BE%E5%AF%A6%E8%BB%9F%E9%AB%94%E9%96%8B%E7%99%BC%E6%BA%96%E5%89%87)
    - [Kanban](https://www.agilealliance.org/glossary/kanban/)
        - [Free eBook](https://activecollab.com/project-management-guides/kanban-ebook)
        - [Kanban 不是任務版](http://kojenchieh.pixnet.net/blog/post/436064977-kanban-%E4%B8%8D%E6%98%AF%E4%BB%BB%E5%8B%99%E7%89%88)
    - [Trello](https://trello.com/)

## Development - Core ##

- [Linux - edX : Introduction to Linux](https://www.edx.org/course/introduction-to-linux)
- [Docker](https://www.docker.com/)
    - [Docker 及 .NET Core](https://docs.microsoft.com/zh-tw/dotnet/core/docker/)

### IDE ###

- [VSCode](https://code.visualstudio.com/)
- [Rider](https://www.jetbrains.com/rider/)
    - [Free eBook : Rider Succinctly](https://www.syncfusion.com/ebooks/rider-succinctly/introduction)

### Backend ###

- [C# Guide](https://docs.microsoft.com/en-us/dotnet/csharp/)
    - [.NET 本事 - C# 本事](https://drive.google.com/open?id=1Xei1yRaZe05AVY_N4KYzxHi9O7rP2fRn)
    - [Free Course : MVA - C# Fundamentals for Absolute Beginners](https://mva.microsoft.com/en-US/training-courses/c-fundamentals-for-absolute-beginners-16169?l=Lvld4EQIC_2706218949) 
    - [Free eBook : C# Succinctly](https://www.syncfusion.com/ebooks/csharp)
- [.NET Documentation](https://docs.microsoft.com/en-us/dotnet/)
- [ASP.NET Documentation](https://docs.microsoft.com/en-us/aspnet/)
- [.NET Core](https://docs.microsoft.com/dotnet/core/)
- [ASP.NET Core](https://docs.microsoft.com/aspnet/core/)
    - [John Wu - ASP.NET Core 2 鐵人賽系列](https://blog.johnwu.cc/tags/it-%E9%82%A6%E5%B9%AB%E5%BF%99-2018-%E9%90%B5%E4%BA%BA%E8%B3%BD/)

### Frontend ###

- [Web essentials - w3shools](https://www.w3schools.com)
    - [HTML - w3shools](https://www.w3schools.com/html/default.asp)
    - [CSS - w3shools](https://www.w3schools.com/css/default.asp)
    - [JS - w3schools](https://www.w3schools.com/js/default.asp)
- [React](https://redux.js.org/)
    - [Babel](https://babeljs.io/)
    - [Redux](https://redux.js.org/)

### Data ###

- [MySQL or MariaDB](https://www.mysql.com/)
    - [SQL - w3shools](https://www.w3schools.com/sql/default.asp)
    - [SQL語法教學](https://www.1keydata.com/tw/sql/sql.html)
- [Redis](https://redis.io/)
    - [Redis University - RU101:Introduction to Redis Data Structures](https://university.redislabs.com/courses/course-v1:redislabs+RU101+2018_03/about)
- [MongoDB](https://www.mongodb.com/)
    - [MongoDB University - M001:MongoDB Basics](https://university.mongodb.com/courses/M001/about)

## Development - Advanced ##

### C# Advanced ###

- [.NET 本事 - 非同步程式設計](https://drive.google.com/open?id=165TKTGh59U6AqC9qA060aTuomtyvDNbH)
- [.NET 本事 - 相依性注入](https://drive.google.com/open?id=1k8tB1dLbxqaIHZSYx9nDR_E0On4AqVDW)

### Unit Test ###

- [Unit Test](https://docs.microsoft.com/zh-tw/dotnet/core/testing/unit-testing-best-practices)
    - [C# NUnit](https://nunit.org)
        - [利用 NUnit 與 .NET Core 進行 C# 單元測試](https://docs.microsoft.com/zh-tw/dotnet/core/testing/unit-testing-with-nunit)
    - [JS Jest](https://jestjs.io/)

### Package ###

- [C# Cake Build](https://cakebuild.net/)
- [NPM Package](https://docs.npmjs.com/about-packages-and-modules)

### CI&CD ###

- [Jenkins](https://jenkins.io/)
- [Kubernetes](https://kubernetes.io/)
    - [edX - Introduction to Kubernetes](https://www.edx.org/course/introduction-to-kubernetes)

## Resource ##

- [SportBetting 電子書櫃](https://drive.google.com/open?id=1JNYblTeI-jPCSD57xRD0YIig_qy8jjBg)

- 線上學習資源：
    - [pluralsight](https://www.pluralsight.com/)
    - [Udemy](https://www.udemy.com/)
    - [Coursera](https://www.coursera.org/)
    - [codecademy](https://www.codecademy.com/)
    - [edX](https://learntocodewith.me/edx)
    - [Microsoft Virtual Academy](https://mva.microsoft.com/)
    - [CodeWar](https://www.codewars.com)
- 學習書籍：
    - [無瑕的程式碼－敏捷軟體開發技巧守則 (Clean Code: A Handbook of Agile Software Craftsmanship)](https://www.tenlong.com.tw/products/9789862017050?list_name=e-106)
    - [無瑕的程式碼－敏捷完整篇－物件導向原則、設計模式與 C#實踐 (Agile principles, patterns, and practices in C#)](https://www.tenlong.com.tw/products/9789864342099?list_name=e-106)
    - [無瑕的程式碼 番外篇－專業程式設計師的生存之道 (The Clean Coder: A Code of Conduct for Professional Programmers)](https://www.tenlong.com.tw/products/9789862017883?list_name=e-106)
    - [無瑕的程式碼－整潔的軟體設計與架構篇 (Clean Architecture: A Craftsman's Guide to Software Structure and Design)](https://www.tenlong.com.tw/products/9789864342945?list_name=e-106)
    - [大話設計模式](https://www.tenlong.com.tw/products/9789866761799)
    - [易讀程式之美學](https://www.tenlong.com.tw/products/9789862767191?list_name=srh)
    - [單元測試的藝術](https://www.tenlong.com.tw/products/9789864342471?list_name=srh)